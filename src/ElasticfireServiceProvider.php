<?php

namespace KaidoRen\Elasticfiresearch;

use Illuminate\Support\ServiceProvider;
use KaidoRen\Elasticfiresearch\Elasticfire;
use Elasticsearch\ClientBuilder;

class ElasticfireServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Elasticfire instance
        $this->app->singleton('elasticfire', function () {
            return new Elasticfire(
                ClientBuilder::create()
                    ->setLogger(ClientBuilder::defaultLogger(storage_path('logs/elastic.log')))
                    ->build()
            );
        });
    }
}
