<?php

namespace KaidoRen\Elasticfiresearch;

use Exception;
use Elasticsearch\Client;
use Illuminate\Database\Eloquent\Model;
use KaidoRen\Elasticfiresearch\ElasticfireBuilder;

class Elasticfire
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get Elasticsearch client.
     * 
     * @return \Elasticsearch\Client
     */
    public function client()
    {
        return $this->client;
    }

    /**
     * Indexing a single item of Eloquent Model.
     * 
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return \KaidoRen\Elasticfiresearch\Elasticfire
     */
    public function index(Model $model)
    {
        $documentData = method_exists($model, 'indexDocumentData')
        ? $model->indexDocumentData() : $model->toArray();

        $params = $this->baseParams($model);
        $params['body'] = $documentData;
        
        $this->client->index($params);

        return $this;
    }

    /**
     * Indexing a all items of Eloquent Model.
     * 
     * @param class $class (Foo::class or 'Foo')
     * @return \KaidoRen\Elasticfiresearch\Elasticfire
     */
    public function indexAll($class, callable $callback = null, $limit = 100)
    {
        if (!is_subclass_of($class, Model::class)) {
            throw new Exception('Invalid class for indexing');
        }

        $class::chunk($limit, function ($items) use ($callback) {
            foreach ($items as $item) {
                if (!is_null($callback) && $callback($item)) {
                    continue;
                }

                $this->index($item);
            }
        });

        return $this;
    }

    /**
     * Deleting a single item of Eloquent Model.
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return \KaidoRen\Elasticfiresearch\Elasticfire
     */
    public function delete(Model $model)
    {
        $params = $this->baseParams($model);
        if ($this->client->exists($params)) {
            $this->client->delete($params);
        }

        return $this;
    }

    /**
     * Deleting a all items of Eloquent Model.
     * 
     * @param class $class (Foo::class or 'Foo')
     * @return \KaidoRen\Elasticfiresearch\Elasticfire
     */
    public function deleteAll($class, callable $callback = null, $limit = 100)
    {
        if (!is_subclass_of($class, Model::class)) {
            throw new Exception('Invalid class for unindexing');
        }

        $class::chunk($limit, function ($items) use ($callback) {
            foreach ($items as $item) {
                if (!is_null($callback) && $callback($item)) {
                    continue;
                }

                $this->delete($item);
            }
        });

        return $this;
    }

    /**
     * Get search query.
     *
     * @param array $options
     * - 'fields' option example: ['fields' => ['title^5', 'name']] (symbol ^ is priority)
     * - 'index' option example: ['index' => $post] ($post is Eloquent Model object).
     * By default all indices are included.
     */
    public function search($keywords, $options = [])
    {
        if (isset($options['index']) && $options['index'] instanceof Model) {
            $params['index'] = $options['index']->getTable();
        }
        
        $params['body']['query']['query_string']['query'] = "*{$keywords}";
        $params['body']['query']['query_string']['fields'] = $options['fields'];
        $params['body']['query']['query_string']['default_operator'] = 'and';

        return new ElasticfireBuilder($this, $keywords, $params);
    }

    protected function baseParams(Model $model, $indexSpecify = true, $identify = true, $limit = null, $offset = null)
    {
        if ($indexSpecify) {
            $index = method_exists($model, 'indexCustomName')
            ? $model->indexCustomName : $model->getTable();

            $params = [
                'index' => $index,
                'type' => get_class($model),
            ];
        }

        !$identify ?: $params['id'] = $model->getKey();
        is_null($limit) ?: $params['size'] = $limit;
        is_null($offset) ?: $params['from'] = $offset;

        return $params;
    }
}
