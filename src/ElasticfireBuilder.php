<?php

namespace KaidoRen\Elasticfiresearch;

use Illuminate\Database\Eloquent\Model;
use KaidoRen\Elasticsfiresearch\Elasticfire;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class ElasticfireBuilder
{
    protected $elasticfire;
    protected $query;
    protected $results;

    public function __construct($elasticfire, $keywords, $query)
    {
        $this->elasticfire = $elasticfire;
        $this->query = $query;
    }

    public function get()
    {
        return $this->search();
    }

    public function paginate($perPage, $page = null, $pageName = 'page')
    {
        $this->query['size'] = $perPage;
        $this->query['from'] = (Paginator::resolveCurrentPage() - 1) * $perPage;
        $collection = $this->search();
        $total = $this->results['hits']['total'];
        return $this->paginator($collection, $total, $perPage, $page, $pageName);
    }

    public function highlight($options = [], $fragmentSize = 300, $maxFragments = 1)
    {
        if (empty($options)) {
            $options = ['pre_tags' => ['<em>'], 'post_tags' => ['</em>']];
        }

        $this->query['body']['highlight']['pre_tags'] = $options['pre_tags'];
        $this->query['body']['highlight']['post_tags'] = $options['post_tags'];

        if (isset($options['fields'])) {
            foreach ($options['fields'] as $field) {
                $this->query['body']['highlight']['fields'][$field]['fragment_size'] = $fragmentSize;
                $this->query['body']['highlight']['fields'][$field]['number_of_fragments'] = $fragmentSize;
            }
        }

        return $this;
    }

    private function search()
    {
        $this->results = $this->elasticfire->client()->search($this->query);
        return $this->collectionResults($this->results);
    }

    private function collectionResults()
    {
        $collection = collect();
        foreach ($this->results['hits']['hits'] as $result) {
            $modelClassName = $result['_type'];

            if (!is_subclass_of($modelClassName, Model::class)) {
                continue;
            }

            $primaryKeyName = $modelClassName::first()->getKeyName();
            $primaryKey = $result['_source'][$primaryKeyName];
            $model = $modelClassName::find($primaryKey);

            if (isset($result['highlight'])) {
                $model->highlight = collect($result['highlight']);
            }

            $collection->push($model);
        }
        return $collection;
    }

    private function paginator($items, $total, $perPage = 15, $page = null, $pageName = 'page')
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items instanceof Collection ?: $items = collect($items);
        return new LengthAwarePaginator($items, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }
}